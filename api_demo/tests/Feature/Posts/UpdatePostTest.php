<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /** @test */

    public function user_can_update_post_if_post_exists_and_data_is_valid()
    {
        $post = Post::factory()->create();
        $dataUpdate = Post::factory()->make()->toArray();

        $respose = $this->putJson(route('posts.update', $post->id), $dataUpdate);

        $respose->assertStatus(Response::HTTP_OK);

        $respose->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn (AssertableJson $json) =>
                $json->where('name', $dataUpdate['name'])->etc()
            )->etc()
        );

        $this->assertDatabaseHas('posts', [
            'name' => $dataUpdate['name'],
            'body' => $dataUpdate['body']
        ]);
    }

    /** @test */
    public function user_can_not_update_post_if_post_exists_and_name_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => '',
            'body' => fake()->text(),
        ];

        $response = $this->putJson(route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')
            )
        );
    }

    /** @test */
    public function user_can_not_update_post_if_post_exists_and_body_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => fake()->name(),
            'body' => '',
        ];

        $response = $this->putJson(route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('body')
            )
        );
    }

    /** @test */
    public function user_can_not_update_post_if_post_exists_and_data_is_not_valid()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => '',
            'body' => '',
        ];

        $response = $this->putJson(route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')->has('body')
            )
        );
    }

    /** @test */
    public function user_can_not_update_post_if_post_not_exists()
    {
        $post_id = -1;
        $dataUpdate = [
            'name' => fake()->name(),
            'body' => fake()->text(),
        ];

        $response = $this->putJson(route('posts.update', $post_id), $dataUpdate);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
