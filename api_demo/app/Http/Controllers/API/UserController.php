<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    protected $user;


    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = $this->user->get();
        $userResource = UserResource::collection($user);

        return $this->sendSuccessResponse($userResource, 'Get list user success', Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {
        $dataCreate = $request->all();
        $user = $this->user->create($dataCreate);

        $userResource = new UserResource($user);
        return $this->sendSuccessResponse($userResource, 'Create user success', Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = $this->user->findOrFail($id);
        $userResource = new UserResource($user);
        return $this->sendSuccessResponse($userResource, 'Show user success', Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $user = $this->user->findOrFail($id);
        $dataUpdate = $request->all();
        $user->update($dataUpdate);
        $userResource = new UserResource($user);

        return $this->sendSuccessResponse($userResource, 'Update user success', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = $this->user->findOrFail($id);
        $user->delete();

        $userResource = new UserResource($user);

        return $this->sendSuccessResponse($userResource, 'Delete user success', Response::HTTP_OK);
    }
}
