<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test */

    public function user_can_delete_post_if_post_exists()
    {
        $post = Post::factory()->create();

        $postBeforeDelete = Post::count();

        $response = $this->deleteJson(route('posts.destroy', $post->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn (AssertableJson $json) =>
                $json->where('name', $post['name'])->etc()
            )->etc()
        );

        $postAfterDelete = Post::count();

        $this->assertEquals($postBeforeDelete - 1, $postAfterDelete);
    }

    /** @test */

    public function user_can_not_delete_post_if_post_not_exists()
    {
        $post_id = -1;
        $postBeforeDelete = Post::count();

        $response = $this->deleteJson(route('posts.destroy', $post_id));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
