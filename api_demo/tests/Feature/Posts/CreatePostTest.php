<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /** @test */

    public function user_can_create_post()
    {

        $dataCreate = Post::factory()->make()->toArray();

        $response = $this->postJson(route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'data',
                fn (AssertableJson $json) =>
                $json->where('name', $dataCreate['name'])->etc()
            )->etc()
        );

        $this->assertDatabaseHas('posts', [
            'name' => $dataCreate['name'],
            'body' => $dataCreate['body'],
        ]);
    }

    public function test_user_can_not_create_post_if_name_is_null()
    {
        $dataCreate = [
            'name' =>  "",
            'body' => fake()->text()
        ];

        $response = $this->postJson(route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')->etc()
            )->etc()
        );
    }

    public function test_user_can_not_create_post_if_body_is_null()
    {
        $dataCreate = [
            'name' =>  fake()->name(),
            'body' => ""
        ];

        $response = $this->postJson(route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('body')->etc()
            )->etc()
        );
    }

    public function test_user_can_not_create_post_if_data_is_not_valid()
    {
        $dataCreate = [
            'name' =>  '',
            'body' => ''
        ];

        $response = $this->postJson(route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $response->assertJson(
            fn (AssertableJson $json) =>
            $json->has(
                'errors',
                fn (AssertableJson $json) =>
                $json->has('name')->has('body')->etc()
            )->etc()
        );
    }
}
